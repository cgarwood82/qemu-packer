# QEMU Packer Examples and Works

This will serve as a repo for my Packer json files for use in
my libvirt lab. Highlights will include mostly just using chef
cookbooks and whatnot. 

There are some statically stored variables that make these not
so portable but are simple enough to hack out for anyone else
who finds them useful. 

# For the love of security...
2 Things to be mindful of:

1. The user trans has an awful password: `password`
Goes without saying, but change this. Be sure to change the 
hash in kickstart file in `/http/` as well! 
